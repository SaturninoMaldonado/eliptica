#include "lola_board.h"

#define VERSION "V2.04"


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//  Version para depuracion de la placa de control de la eliptica2.01
//  Placa de control de la plataforma LOLA :  PCB LOLA V2.01
//
//  Son necesarios los siguientes cambios:
//  - Cortocircuitar NA-COM rel Izquierdo (para motor izquierdo)
//  - Cortocircuitar AUX1 pin 3 y 4 
//  - Cortocircuitar AUX1 pin 2 con pin 1 conector RJ45
//  - Cortocircuitar R22 y R23 de sensor de corriente L298

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#define IGNORE_PULSE   0 // time in micros to ignore encoder pulses if faster


unsigned char velIZQ,velDER;
//unsigned char flag_move_motor=0;


unsigned int SPEED_INI_L=255;  
unsigned int SPEED_INI_R=255;  

unsigned int veloc_right;
unsigned int veloc_left;


// Direction of movement
unsigned char dir_right,dir_left;

// Variables to keep each encoder pulse
volatile unsigned int encoderIZQ = 0, encoderDER = 0;

//These variables keep track of the pulses received with a delay shorter than IGNORE_PULSE1 microseconds
volatile unsigned int ignored_left = 0, ignored_right = 0;

// Variables to obtain robot position and orientation (X, Y Theta)
unsigned int aux_encoderIZQ = 0, aux_encoderDER = 0;
volatile signed int encoder = 0;

// Auxiliar variables to filter false impulses from encoders
volatile unsigned long auxr=0,auxl=0;
// Auxiliar variables to keep micros() at encoders
unsigned long tr,tl;

// Indicate the PWM that is applied to the motors
int velr = SPEED_INI_R;
int vell = SPEED_INI_L;
int encoder_ant;

unsigned int pid_target_rpm;

unsigned char flag=1;

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
void setup() 
{

  
  // Add the interrupt lines for encoders
  // Motor LOLA
  attachInterrupt(0, cuentaDER, CHANGE);  
  attachInterrupt(1, cuentaIZQ, CHANGE);
  // Motor MAXON
  attachInterrupt(0, cuentaDER, RISING);  
  attachInterrupt(1, cuentaIZQ, RISING);

  //Battery pin for voltaje measurement
  pinMode(BAT_PIN,         INPUT);

  //Dip switch for configuration
  pinMode(SW1_PIN,  INPUT_PULLUP);
  pinMode(SW2_PIN,  INPUT_PULLUP);
  pinMode(SW3_PIN,  INPUT_PULLUP);
  pinMode(SW4_PIN,  INPUT_PULLUP);
  pinMode(SW5_PIN,  INPUT_PULLUP);
  pinMode(SW6_PIN,  INPUT_PULLUP);
  pinMode(SW7_PIN,  INPUT_PULLUP);
  pinMode(SW8_PIN,  INPUT_PULLUP);

  //Buzzer
  pinMode(BUZZER_PIN, OUTPUT);

  // set all the motor control pins to outputs
  pinMode(MOT_R_PWM_PIN, OUTPUT);
  pinMode(MOT_L_PWM_PIN, OUTPUT);
  
  pinMode(MOT_R_A_PIN, OUTPUT);
  pinMode(MOT_R_B_PIN, OUTPUT);
  pinMode(MOT_L_A_PIN, OUTPUT);
  pinMode(MOT_L_B_PIN, OUTPUT);

  // set encoder pins to inputs
  pinMode(MOT_L_ENC_B_PIN, INPUT);
  pinMode(MOT_R_ENC_B_PIN, INPUT);

  //L RGB LED
  pinMode(L_RED_PIN,      OUTPUT);
  pinMode(L_GRE_PIN,      OUTPUT);
  pinMode(L_BLU_PIN,      OUTPUT);
  
  //R RGB LED
  pinMode(R_RED_PIN,      OUTPUT);
  pinMode(R_GRE_PIN,      OUTPUT);
  pinMode(R_BLU_PIN,      OUTPUT);

  //F Ultrasound sensor
  pinMode(F_US_TRIG,      OUTPUT);
  pinMode(F_US_ECHO,      INPUT);
  //L Ultrasound sensor
  pinMode(L_US_TRIG,      OUTPUT);
  pinMode(L_US_ECHO,      INPUT);
  //R Ultrasound sensor           
  pinMode(R_US_TRIG,      OUTPUT);
  //  pinMode(R_US_ECHO,      INPUT); Pin 53 (coincide con el pin CAN necesario en Mega)
  //B Ultrasound sensor
  pinMode(B_US_TRIG,      OUTPUT);
  pinMode(B_US_ECHO,      INPUT);
  
  // set buttons pins
  pinMode(PIN_FORWARD, INPUT_PULLUP);
  pinMode(PIN_BACKWARD, INPUT_PULLUP);
  pinMode(PIN_LEFT, INPUT_PULLUP);
  pinMode(PIN_RIGHT, INPUT_PULLUP);
  
  pinMode(LED, OUTPUT);

  digitalWrite(BUZZER_PIN, LOW);

  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_R_B_PIN, LOW);    
  digitalWrite(MOT_L_A_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);

  analogWrite(MOT_R_PWM_PIN, 0);
  analogWrite(MOT_L_PWM_PIN, 0);

  Serial2.begin(38400);      //init the serial port
//  Serial.begin(38400);      //init the serial port
  Serial.begin(115200);      //init the serial port
  

}  // End of setup()


//////////////////////////////////////////////////
//  Right encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaDER()
{
  tr=micros();
  // if pulse is too fast from previoius is ignored
//    Serial.println(tr-auxr);
  if (tr-auxr>(unsigned long)IGNORE_PULSE)
  {
    
    auxr=tr;
    encoderDER++;    //Add one pulse
  }
  
}  // end of cuentaDER

//////////////////////////////////////////////////
//  Left encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaIZQ()
{
  tl=micros();
  // if pulse is too fast from previoius is ignored
  if (tl-auxl>(unsigned long)IGNORE_PULSE)
  {
    auxl=tl;
    encoderIZQ++;  //Add one pulse
  }
}  // end of cuentaIZQ


///////////////////////////////////////////
//              MOVE MOTORS              //
//  dir_right (1: foward / 0: backwards) //
//  dir_left  (1: foward / 0: backwards) //
///////////////////////////////////////////
void move_motors() 
{
  // now turn off motors
  // Adaptation for L298n
  unsigned int inhib_r = 0xBB, inhib_l = 0xEE;

  encoderIZQ = ignored_left = aux_encoderIZQ ;
  encoderDER =  ignored_right =  aux_encoderDER; 
  
  encoder = encoder_ant = 0;

  //Deactivate both motor's H-bridge
  PORTA &= 0xAA;

  velr = SPEED_INI_R;
  vell = SPEED_INI_L;

  if (!velr)
    PORTB |= 0x08;            
  else {
    inhib_r |= 0x44;
    analogWrite(MOT_R_PWM_PIN, velr);
  }

  if (!vell)
    PORTB |= 0x10;             

  else {
    inhib_l |= 0x11;
    analogWrite(MOT_L_PWM_PIN, vell);
  }  

  if (dir_right && dir_left)
    PORTA |= 0x05 & inhib_r & inhib_l;

  else if (!dir_right && dir_left)
    PORTA |= 0x41 & inhib_r & inhib_l;

  else if (dir_right && !dir_left)
    PORTA |= 0x14 & inhib_r & inhib_l;
  
  else
    PORTA |= 0x50 & inhib_r & inhib_l;
    
//  Serial.println("move_motors");

}  // End of move_motors()


///////////////////
//  STOP_MOTORS  //
///////////////////
void stop_motors() 
{
  PORTB |= 0x3 << 3;                              
  PORTA &= 0xAA;
  //We will fix the same duty cycle for the PWM outputs to make the braking spped equal!
  analogWrite(MOT_R_PWM_PIN, 255);
  analogWrite(MOT_L_PWM_PIN, 255);
  //delay(300);                             ////////////////////////////////////////////////////////////////////////////////////////////////////////
}  // End of stop_motors()
void loop() 
{
  unsigned int contador=0;
  char cadena[100];
  unsigned long temp;
  unsigned long aux_temp;
  float rpm;
  unsigned int tiempo;
  unsigned int pulsos;
  
  if (flag)
  {
    velr = 125;
    vell = 125;
    SPEED_INI_L=255;
    SPEED_INI_R=255;
    Serial.println(" MUEVE MOTOR");
    move_motors();
    flag=0;
  }
  else
  {
    Serial.println(" PARA MOTOR");
    stop_motors();
    flag=1;
    delay(1000);
    velr = 125;
    vell = 125;
    SPEED_INI_L=255;
    SPEED_INI_R=255;
    Serial.println(" MUEVE MOTOR");
    move_motors();
    flag=0;
  }
  while (contador<255)
  {
    sprintf(cadena,"%03d",contador);
    Serial.print(cadena);
    Serial.print("   Lectura del potenciometro: ");
    pid_target_rpm = map(analogRead(A11), 0, 1023, 12, 160);  //Read the pot at A0, filter it and map its range (0-1023) to typcal 
    sprintf(cadena,"%03d",pid_target_rpm);
    Serial.print(cadena);
    Serial.print("  Lectura del encoder: ");
    sprintf(cadena,"%03d",(unsigned int)encoderIZQ);
    temp=micros();
    tiempo=(unsigned int)((unsigned long)temp-(unsigned long)aux_temp);
    tiempo=tiempo/1000; // En ms
    pulsos=(unsigned int)encoderIZQ-(unsigned int)aux_encoderIZQ;
    rpm=(float)1000*((float)pulsos*(float)60/((float)25*(float)256*(float)tiempo));
    aux_encoderIZQ=encoderIZQ;
    aux_temp=temp;
    Serial.print(cadena);
    Serial.print("  Pulsos: ");
    Serial.print(pulsos);
    Serial.print("  T: ");
    Serial.print(tiempo);
    Serial.print(" RPM ");
    Serial.print(rpm);
    
    vell=contador;
    Serial.print(" vell ");
    Serial.println(vell);
    analogWrite(MOT_L_PWM_PIN, vell);
    delay(50);

    contador++;
  }

}  // fin del loop


