#include "lola_board.h"

#define VERSION "V2.04"

// Conditional compilation
#define DEBUG         //Enable debugging features WARNING: Might slow program

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//  Version para depuracion de la placa de control de la eliptica2.01
//  Placa de control de la plataforma LOLA :  PCB LOLA V2.01
//
//  Son necesarios los siguientes cambios:
//  - Cortocircuitar NA-COM rel Izquierdo (para motor izquierdo)
//  - Cortocircuitar AUX1 pin 3 y 4 
//  - Cortocircuitar AUX1 pin 2 con pin 1 conector RJ45
//  - Cortocircuitar R22 y R23 de sensor de corriente L298

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#define IGNORE_PULSE   800 // time in micros to ignore encoder pulses if faster


unsigned char velIZQ,velDER;
//unsigned char flag_move_motor=0;


unsigned int SPEED_INI_L=255;  
unsigned int SPEED_INI_R=255;  

unsigned int veloc_right;
unsigned int veloc_left;


// Direction of movement
unsigned char dir_right,dir_left;

// Variables to keep each encoder pulse
volatile unsigned int encoderIZQ = 0, encoderDER = 0;

//These variables keep track of the pulses received with a delay shorter than IGNORE_PULSE1 microseconds
volatile unsigned int ignored_left = 0, ignored_right = 0;

// Variables to obtain robot position and orientation (X, Y Theta)
unsigned int aux_encoderIZQ = 0, aux_encoderDER = 0;
volatile signed int encoder = 0;

// Auxiliar variables to filter false impulses from encoders
volatile unsigned long auxr=0,auxl=0;
// Auxiliar variables to keep micros() at encoders
unsigned long tr,tl;

// Indicate the PWM that is applied to the motors

int encoder_ant;

unsigned int pid_target_rpm;
 double pid_in_rpm=0;      //PID controller's input, in rpm
unsigned char flag=1;
unsigned char new_pulse=0; // flag to detect no pulse and stop the motor
   
    
     float salida_float=12;
  double error_ant=0;
  unsigned int velr,vell;
  unsigned char flag_mov=0;
  
  unsigned long now, tref;
  unsigned char flag_primera_vez=1;
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
void setup() 
{
  int lectura;
  
  // Add the interrupt lines for encoders
  attachInterrupt(0, cuentaDER, CHANGE);
  
  attachInterrupt(1, cuentaIZQ, CHANGE);

  //Battery pin for voltaje measurement
  pinMode(BAT_PIN,         INPUT);

  //Dip switch for configuration
  pinMode(SW1_PIN,  INPUT_PULLUP);
  pinMode(SW2_PIN,  INPUT_PULLUP);
  pinMode(SW3_PIN,  INPUT_PULLUP);
  pinMode(SW4_PIN,  INPUT_PULLUP);
  pinMode(SW5_PIN,  INPUT_PULLUP);
  pinMode(SW6_PIN,  INPUT_PULLUP);
  pinMode(SW7_PIN,  INPUT_PULLUP);
  pinMode(SW8_PIN,  INPUT_PULLUP);

  //Buzzer
  pinMode(BUZZER_PIN, OUTPUT);

  // set all the motor control pins to outputs
  pinMode(MOT_R_PWM_PIN, OUTPUT);
  pinMode(MOT_L_PWM_PIN, OUTPUT);
  
  pinMode(MOT_R_A_PIN, OUTPUT);
  pinMode(MOT_R_B_PIN, OUTPUT);
  pinMode(MOT_L_A_PIN, OUTPUT);
  pinMode(MOT_L_B_PIN, OUTPUT);

  // set encoder pins to inputs
  pinMode(MOT_L_ENC_B_PIN, INPUT);
  pinMode(MOT_R_ENC_B_PIN, INPUT);

  //L RGB LED
  pinMode(L_RED_PIN,      OUTPUT);
  pinMode(L_GRE_PIN,      OUTPUT);
  pinMode(L_BLU_PIN,      OUTPUT);
  
  //R RGB LED
  pinMode(R_RED_PIN,      OUTPUT);
  pinMode(R_GRE_PIN,      OUTPUT);
  pinMode(R_BLU_PIN,      OUTPUT);

  //F Ultrasound sensor
  pinMode(F_US_TRIG,      OUTPUT);
  pinMode(F_US_ECHO,      INPUT);
  //L Ultrasound sensor
  pinMode(L_US_TRIG,      OUTPUT);
  pinMode(L_US_ECHO,      INPUT);
  //R Ultrasound sensor           
  pinMode(R_US_TRIG,      OUTPUT);
  //  pinMode(R_US_ECHO,      INPUT); Pin 53 (coincide con el pin CAN necesario en Mega)
  //B Ultrasound sensor
  pinMode(B_US_TRIG,      OUTPUT);
  pinMode(B_US_ECHO,      INPUT);
  
  // set buttons pins
  pinMode(PIN_FORWARD, INPUT_PULLUP);
  pinMode(PIN_BACKWARD, INPUT_PULLUP);
  pinMode(PIN_LEFT, INPUT_PULLUP);
  pinMode(PIN_RIGHT, INPUT_PULLUP);
  
  pinMode(LED, OUTPUT);

  digitalWrite(BUZZER_PIN, LOW);

  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_R_B_PIN, LOW);    
  digitalWrite(MOT_L_A_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);

  analogWrite(MOT_R_PWM_PIN, 0);
  analogWrite(MOT_L_PWM_PIN, 0);
  
  Serial.begin(38400);      //if debuggin mode is enable, init the serial port
  //if debug mode is enable, init the uart to talk to the pc
  #ifdef DEBUG
       Serial.println("");
       Serial.println("Esperando a que el potencietro llegue a bajo nivel");
  #endif

  //Only start if the selected speed is lower than 12 RMP
  lectura=100;
  while ( lectura> 10)
  {
    lectura=map(analogRead(A11), 0, 1023, 0, 90);
    Serial.print("Esperando a que el potencietro llegue a bajo nivel 111  ");
    Serial.println(lectura);
  }

  
  //Only start if the selected speed is lower than 12 RMP
  while (lectura < 25)
  {
    lectura=map(analogRead(A11), 0, 1023, 0, 90);
    Serial.print("Esperando a que el potencietro llegue a bajo nivel 222  ");
    Serial.println(lectura);
  }

}  // End of setup()


//////////////////////////////////////////////////
//  Right encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaDER()
{
  tr=micros();
  // if pulse is too fast from previoius is ignored
//    Serial.println(tr-auxr);
  if (tr-auxr>(unsigned long)IGNORE_PULSE)
  {
    new_pulse=1;
    auxr=tr;
    encoderDER++;    //Add one pulse
  }
  
}  // end of cuentaDER

//////////////////////////////////////////////////
//  Left encoder interrupt using only one pin
//////////////////////////////////////////////////
void cuentaIZQ()
{
  tl=micros();
  // if pulse is too fast from previoius is ignored
  if (tl-auxl>(unsigned long)IGNORE_PULSE)
  {
    new_pulse=1;
    auxl=tl;
    encoderIZQ++;  //Add one pulse
  }
}  // end of cuentaIZQ


///////////////////////////////////////////
//              MOVE MOTORS              //
//  dir_right (1: foward / 0: backwards) //
//  dir_left  (1: foward / 0: backwards) //
///////////////////////////////////////////
void move_motors() 
{
  // now turn off motors
  // Adaptation for L298n
  unsigned int inhib_r = 0xBB, inhib_l = 0xEE;

  encoderIZQ = ignored_left = aux_encoderIZQ ;
  encoderDER =  ignored_right =  aux_encoderDER; 
  
  encoder = encoder_ant = 0;

  //Deactivate both motor's H-bridge
  PORTA &= 0xAA;

  velr = SPEED_INI_R;
  vell = SPEED_INI_L;

  if (!velr)
    PORTB |= 0x08;            
  else {
    inhib_r |= 0x44;
    analogWrite(MOT_R_PWM_PIN, velr);
  }

  if (!vell)
    PORTB |= 0x10;             

  else {
    inhib_l |= 0x11;
    analogWrite(MOT_L_PWM_PIN, vell);
  }  

  if (dir_right && dir_left)
    PORTA |= 0x05 & inhib_r & inhib_l;

  else if (!dir_right && dir_left)
    PORTA |= 0x41 & inhib_r & inhib_l;

  else if (dir_right && !dir_left)
    PORTA |= 0x14 & inhib_r & inhib_l;
  
  else
    PORTA |= 0x50 & inhib_r & inhib_l;
    
//  Serial.println("move_motors");
flag_mov=1;

}  // End of move_motors()


///////////////////
//  STOP_MOTORS  //
///////////////////
void stop_motors() 
{
  PORTB |= 0x3 << 3;                              
  PORTA &= 0xAA;
  //We will fix the same duty cycle for the PWM outputs to make the braking spped equal!
  analogWrite(MOT_R_PWM_PIN, 255);
  analogWrite(MOT_L_PWM_PIN, 255);
  //delay(300);                             ////////////////////////////////////////////////////////////////////////////////////////////////////////
  flag_mov=0;
  
}  // End of stop_motors()

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
void loop() 
{
  static int reset_count=0;
    unsigned char salida;
  double error;
    
  if (new_pulse) 
  {
    //unset the data avaiable flag
    new_pulse=false;
    reset_count=0;
  }
  else
  {
    reset_count++;
    // If there is no new pulse for a while the system is reset
    if (reset_count>800)
    {
      Serial.println("  ");
      Serial.println("Bloqueado por falta de pulsos");
      
      //Send pid's output to the real world
      while(1)
      {
        // Fix Blue led show error because no pulses are detected
        digitalWrite(13, 1);
        stop_motors();
      }      
    }
  }

  //PID INPUT CALCULATION
  //compute rpm
    now=millis();
    now=now/8;
    if (now-tref>50)
      {
        pid_target_rpm = map(analogRead(A11), 0, 1023, 12, 90);  //Read the pot at A0, filter it and map its range (0-1023) to typcal 
        
        Serial.print("  Encoder  ");
        Serial.print(encoderIZQ-aux_encoderIZQ);
/*        // Motor 24 V LOLA
        // PID tunning
        #define PID_KP  0.4
        #define PID_KD  0.4
        pid_in_rpm=((double)encoderIZQ-(double)aux_encoderIZQ)*(double)60/  // 60 seg por minuto
                   ((double)305.6*(double)(now-tref)*(double)1E-3);         // 305.6 pulsos por vuelta
*/                                                                            // 50.9 (reductora) x 3 pulsos por vuelta x 2 flancos
        // Motor 12 V LOLA
        // PID tunning
        #define PID_KP  2.5
        #define PID_KD  0.0
        pid_in_rpm=((double)encoderIZQ-(double)aux_encoderIZQ)*(double)60/  // 60 seg por minuto
                   ((double)7200*(double)(now-tref)*(double)1E-3);         // 305.6 pulsos por vuelta
                                                                            // 50.9 (reductora) x 3 pulsos por vuelta x 2 flancos
                                                                            
        Serial.print("  ");
        Serial.print(now-tref);
        Serial.print("   \t");
        tref=now;
        aux_encoderIZQ=encoderIZQ;

        if (flag_primera_vez)
        {  
          pid_in_rpm=pid_target_rpm;
          flag_primera_vez=0;
          error_ant=0;
        }    
        error=(pid_target_rpm-pid_in_rpm);
        salida_float=salida_float+error*PID_KP+(error-error_ant)*PID_KD;
        Serial.print(" Error ");
        Serial.print(error);
        error_ant=error;
        if (salida_float>255)
          salida_float=255;
        else
          if (salida_float<40)
            salida_float=40;
    
        salida=salida_float;
        if (flag_mov)
          analogWrite(MOT_L_PWM_PIN, salida);
        else
          move_motors();
    
        //If debuggin output is enabled, echo current status to serial term
        #ifdef DEBUG              //debug terminal out
          Serial.print("  Motor RPM: ");
          Serial.print(pid_in_rpm);
          Serial.print("\tTGT: ");
          Serial.print(pid_target_rpm );
          Serial.print("\tPWM OUT: ");
          Serial.println(salida);
        #endif
      }
}//end loop()
  
/*  
  unsigned int contador=0;
  char cadena[100];
  
  if (flag)
  {
    velr = 125;
    vell = 125;
  SPEED_INI_L=255;
  SPEED_INI_R=255;
    Serial.println(" MUEVE MOTOR");
    move_motors();
    flag=0;
  }
  else
  {
    Serial.println(" PARA MOTOR");
    stop_motors();
    flag=1;
  }
  while (contador<255)
  {
    sprintf(cadena,"%03d",contador);
    Serial.print(cadena);
    Serial.print("   Lectura del potenciometro: ");
    pid_target_rpm = map(analogRead(A11), 0, 1023, 12, 160);  //Read the pot at A0, filter it and map its range (0-1023) to typcal 
    sprintf(cadena,"%03d",pid_target_rpm);
    Serial.print(cadena);
    Serial.print("  Lectura del encoder: ");
    sprintf(cadena,"%03d",encoderIZQ);
    Serial.println(cadena);
    vell=contador;
    analogWrite(MOT_L_PWM_PIN, vell);
    delay(50);

    contador++;
  }

}
*/
/*
void loop() 
{
  // put your main code here, to run repeatedly:
  STATE=RESET_STATE;
  
  Serial.println("ELIPTICA MEGA");
  dir_right = dir_left = 1;
  SPEED_INI_L=255;
  SPEED_INI_R=255;
  move_motors();

  delay(2000);  
  stop_motors();
  delay(1000);  

//       Lola();
       return;       

}*/
