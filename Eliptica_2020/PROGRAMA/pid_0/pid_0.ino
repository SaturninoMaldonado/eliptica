//******************************************************************************************************//
// Config section                                                                                       //
//******************************************************************************************************//
  float salida_float=12;
  double error_ant=0;

  //  I/O PINS
    #define PWM_PIN 3   //H-bridge pwm signal is at pin 9
    #define HA_PIN 2    //Encoder HA signal is connected to pin 2 (EINT0)
    #define POT_PIN 0   //Potenciometer analog pin
    
  // PID tunning
    #define PID_KP  0.4
    #define PID_KI  0
    #define PID_KD  0.2
    
  // Encoder parameters
    #define ENC_PPR 3   //how many pulse the encoder sends over a revolution

  // Conditional compilation
    #define DEBUG         //Enable debugging features WARNING: Might slow program

//******************************************************************************************************//
// Include section                                                                                      //
//******************************************************************************************************//
//  #include <PID_v1.h>   //The PID library

//******************************************************************************************************//
// global variables definition                                                                          //
//******************************************************************************************************//
  //PID related variables
    double pid_in_rpm=0;      //PID controller's input, in rpm
    double pid_out_pwm=0;     //PID controller's output, as a pwm signal
    double pid_target_rpm=0;  //PID controller's setpoint, in rpm

    int contador=0;           // Counter for a Blue LED blink
    int reset_count=0;        // Counter to reset output if no pulses are detected
    
  
  //Encoder relate variables
    volatile boolean  dir=false;                //Direction of rotation TRUE if fwd, false if rwd
    volatile boolean  new_pulse=false;          //Flag. Set when a new pulse is recived
    volatile unsigned long  last_ha_time=0;     //Stores when the last encoder pulse was received
    volatile unsigned long  raw_ha_t=0;         //The unfiltered period of the last received pulse
    volatile unsigned long  filtered_ha_t=0;    //Smoothed period 
  
  //System variables
  volatile unsigned long  last_uart_sent=5000;  //The time of the last uart communication


//******************************************************************************************************//
// Setup function                                                                                       //
//******************************************************************************************************//
void setup() 
{
  //Set PWM and DIR pins as Output
  pinMode(PWM_PIN, OUTPUT);       //set PWM pin as out
  pinMode(13, OUTPUT);       //set dir pin as out
  
  //Stop the motor, and set correct direction
  digitalWrite(PWM_PIN, LOW);       //Stop the motor
  
  // Configure HA pin to generate an interrupt (EINT0) on a rising edge
//  attachInterrupt(digitalPinToInterrupt(HA_PIN), _ha_ISR, FALLING);
  attachInterrupt(1, _ha_ISR, FALLING);


  //Configure the PID
  // PID variable init
  pid_target_rpm  = 0;    //This is pid's SETPOINT, expresed in motor's desired speed in RPM (up to ~75 RPM) 
  pid_in_rpm      = 0;    //This is pid's INPUT, expressed in RPM calculated from the period of an encoder pulse
  pid_out_pwm     = 0;    //This is pid's OUTPUT. It's a PWM signal rangin from 0 to 254 (at 255 the H-bridge glitchs)

      
  //if debug mode is enable, init the uart to talk to the pc
    #ifdef DEBUG
       Serial.begin(38400);      //if debuggin mode is enable, init the serial port
       Serial.println("");
    #endif

  //Only start if the selected speed is lower than 12 RMP
  while (map(analogRead(POT_PIN), 0, 1023, 0, 90) > 5)
    ;

}  //end setup

//******************************************************************************************************//
// main loop                                                                                            //
//******************************************************************************************************//
void loop() 
{
  unsigned char salida;
  double error;
    
  //Read the potenciometer, and update PID SetPoint
  pid_target_rpm = map(analogRead(POT_PIN), 0, 1023, 12, 160);  //Read the pot at A0, filter it and map its range (0-1023) to typcal motor speeds (0-80)

 
//  pid_target_rpm = 90;
  
  //PID INPUT CALCULATION
  if (new_pulse) 
  {
    //unset the data avaiable flag
    new_pulse=false;
    //compute rpm
    pid_in_rpm =(double)1 / ( (double)raw_ha_t * (double)1E-6 );
    reset_count=0;
  }
  else
  {
    reset_count++;
    // If there is no new pulse for a while the system is reset
    Serial.println(reset_count);
    if (reset_count>100)
    {
      //Send pid's output to the real world
      while(1)
      {
        // Fix Blue led show error because no pulses are detected
        digitalWrite(13, 1);
        analogWrite(PWM_PIN, 0);
      }      
    }
  }

  error=(pid_target_rpm-pid_in_rpm);
  salida_float=salida_float+error*PID_KP+(error-error_ant)*PID_KD;
  error_ant=error;
  if (salida_float>255)
    salida_float=255;
  else
    if (salida_float<12)
      salida_float=12;

  salida=salida_float;
  delay(50);
  analogWrite(PWM_PIN, salida);


  //If debuggin output is enabled, echo current status to serial term
  #ifdef DEBUG              //debug terminal out
    Serial.print("RPM: ");
    Serial.print(pid_in_rpm);
    Serial.print("\tTGT: ");
    Serial.print(pid_target_rpm );
    Serial.print("\tOUT: ");
//    Serial.print(pid_out_pwm);
    Serial.print(salida);
    Serial.print("\tDir: ");
    Serial.print(dir);
    Serial.print("\n");
  #endif

}//end loop()

//******************************************************************************************************//
// ENCODER pulse ISR                                                                                    //
//******************************************************************************************************//
void _ha_ISR (void)
{
  unsigned long now;

  //*****************// 
  //     PERIOD      //
  //*****************//
  //stores the current time
  now = micros();

  if (now-last_ha_time>1000)
  {
    //Calculates the perdiod and flag for a new value.
    raw_ha_t = (now - last_ha_time) * ENC_PPR;
    new_pulse = true;
  
    //Stores the in the global var
    last_ha_time = now;
    // end period calculation

    contador++;
    if (contador>25)
    {
      // Blink proportional to the speed
      contador=0;
      digitalWrite(13, !digitalRead(13));
    }
  }
}//end _ha_ISR
